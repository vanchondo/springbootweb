package com.vanchondo.springBootWeb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class HomeController {

    @GetMapping("")
    public String index(){
        return "index";
    }

    @GetMapping("welcome/{user}")
    public String welcomeUser(@PathVariable("user") String user, Model model){
        model.addAttribute("user", user);
        return "welcome";
    }
}
