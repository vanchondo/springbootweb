## Requirements
* JDK 8
* Maven 3.5

## Building the application
In project's root folder run:
* mvn clean compile package

## Starting the application
In project's root folder run:
* java -jar target/springBootWeb-1.0-SNAPSHOT.jar

##
http://www.vanchondo.com